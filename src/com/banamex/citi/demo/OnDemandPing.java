package com.banamex.citi.demo;

import java.util.Enumeration;

public class OnDemandPing {
	 
	  private final static String SERVER_NAME="mydocserver";//instanciar con el nombre del server 
	  private final static String USER_ID="my_username"; //id de usuario proporcionado por citi
	  private final static String PASSWORD="myPassword"; //contrasenia proporcionada
	  
	  private final static int PORT = 1445; //puerto por default, si fuese otro modificar
	  
	  public void initConfig() {
		  //Configura una nueva conexion con OnDemand con la configuracion por default
		  ODServer odServer = new ODServer(new ODConfig ());
		  
		  try {
			  //Configura las credenciales y propiedades en el server
			  odServer.setConnectType(ODConstant.CONNECT_TYPE_TCPIP);
			  odServer.setServerName(SERVER_NAME);
			  odServer.setPort(PORT);
			  odServer.setUserId(USER_ID);
			  odServer.setPassword(PASSWORD);
			  
			  /* Inicializa la conexion al servidor OnDemand,
			     una vez incializada, la conexion debe ser terminada cuando
			     sea finalice  
			     */ 
			  odServer.initialize("ODPing");
			  
			  //logon
			  
			  odServer.logon();
			  
			  //
			  //Reporte del status del ping
			  //
			  
			  System.out.println("OnDemand server " +
					             odServer.getServerName()+ " respondiendo con exito");
			  //Realiza una funcion util
			  
			  listFolders(odServer);
			  
			  
		  }
		  
		  catch(ODException e) {
			  if(e.getErrorId() == 2107) {
				  System.out.println("OnDemand server"+ 
						  odServer.getServerName()+" respondiendo pero...");
				  System.out.println(e.getErrorMsg());
			  }
			  else {
				  System.out.println("Error encontrado "+e.getErrorMsg());
				  System.out.println("Error code "+ e.getErrorId());
			  }
		  }
		  
		  catch(Exception e) {
			  e.printStackTrace();			  
		  }
		  finally {
			  //Aseguramos el logout del usuario
			  
			  try { odServer.logoff(); }
			  catch(Exception e) {/*Ignorar cualquier otro problema*/ }
			  
			  //Siempre termina todas las conexiones que no son mas necesarias
			  odServer.terminate();
		  }
	  }
	  
	  public static void listFolders(ODServer odServer) throws Exception {
		  Enumeration en =odServer.getFolders();
		  while(en.hasMoreElements()) {
			  System.out.println("Folder: "+((ODFolder) en.nextElement()).getName());
		  }
	  }
	}
